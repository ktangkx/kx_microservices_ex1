.u.upd:{[x;y] upd (x;y) }
initStream: .qsp.read.fromCallback[`upd] .qsp.split[]
trStream: initStream .qsp.filter[{ `trade ~ first x }] .qsp.map[{last x}] .qsp.write.toStream[`trade;`data]
quStream: initStream .qsp.filter[{ `quote ~ first x }] .qsp.map[{last x}] .qsp.write.toStream[`quote;`data]
.qsp.run (quStream;trStream)