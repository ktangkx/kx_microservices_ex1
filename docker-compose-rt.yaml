
networks:
  kx:
    name: kx

services:
  # RT services - these are different services (not replicas), each with its own volume
  rt-data-0:
    image: ${kxi_rt_single}
    # Need to set hostname so rt nodes/publishers/subcribers can find each other
    hostname: rt-data-0
    container_name: rt-data-0
    command: ['-p','6000','-in','${in_dir}','-out','${out_dir}','-cp','${state_dir}','-size','${size}','-limit','${limit}','-time','${time}','-disk','${disk}']
    # Have to run as root (default is nobody) in order to write to docker volume
    user: root
    restart: unless-stopped
    networks: [kx]
    environment:
      - KDB_LICENSE_B64
      - RT_TOPIC_PREFIX
      - RT_SINK
      - RAFT_HEARTBEAT
      - RAFT_LOG_SIZE
      - RAFT_CHUNK_SIZE
      - RT_LOGLEVEL_CONSOLE
      - RT_SEQ_SESSION_PATH
      - RT_EXTERN_PREFIX
      - RT_QURAFT_LOG_LEVEL
      - RT_LOG_LEADER    
    ports:
      - 6000:6000
      - 127.0.0.1:5000:5000/tcp
      - 127.0.0.1:5001:5001/tcp
      - 127.0.0.1:5002:5002/tcp
    volumes:
      - ./data/rt/rt-data-0:/s
      - ./data/rt/rt-data-session:/rt-session
      - ${local_dir}:${mnt_dir}
      - ./data/rt/node0:/tmp/node

  rt-data-1:
    image: ${kxi_rt_single}
    # Need to set hostname so rt nodes/publishers/subcribers can find each other
    hostname: rt-data-1
    container_name: rt-data-1
    command: ['-p','6000','-in','${in_dir}','-out','${out_dir}','-cp','${state_dir}','-size','${size}','-limit','${limit}','-time','${time}','-disk','${disk}']    
    user: root
    restart: unless-stopped
    networks: [kx]
    environment:
      - KDB_LICENSE_B64
      - RT_TOPIC_PREFIX
      - RT_SINK
      - RAFT_HEARTBEAT
      - RAFT_LOG_SIZE
      - RAFT_CHUNK_SIZE
      - RT_LOGLEVEL_CONSOLE
      - RT_SEQ_SESSION_PATH
      - RT_EXTERN_PREFIX
      - RT_QURAFT_LOG_LEVEL
      - RT_LOG_LEADER    
    ports:
      - 127.0.0.2:5000:5000/tcp
      - 127.0.0.2:5001:5001/tcp
      - 127.0.0.2:5002:5002/tcp
    volumes:
      - ./data/rt/rt-data-1:/s      
      - ./data/rt/rt-data-session:/rt-session
      - ${local_dir}:${mnt_dir}
      - ./data/rt/node1:/tmp/node

  rt-data-2:
    image: ${kxi_rt_single}
    # Need to set hostname so rt nodes/publishers/subcribers can find each other
    hostname: rt-data-2
    container_name: rt-data-2
    command: ['-p','6000','-in','${in_dir}','-out','${out_dir}','-cp','${state_dir}','-size','${size}','-limit','${limit}','-time','${time}','-disk','${disk}']    
    user: root
    restart: unless-stopped
    networks: [kx]
    environment:
      - KDB_LICENSE_B64
      - RT_TOPIC_PREFIX
      - RT_SINK
      - RAFT_HEARTBEAT
      - RAFT_LOG_SIZE
      - RAFT_CHUNK_SIZE
      - RT_LOGLEVEL_CONSOLE
      - RT_SEQ_SESSION_PATH
      - RT_EXTERN_PREFIX
      - RT_QURAFT_LOG_LEVEL
      - RT_LOG_LEADER    
    ports:
      - 127.0.0.3:5000:5000/tcp
      - 127.0.0.3:5001:5001/tcp
      - 127.0.0.3:5002:5002/tcp
    volumes:
      - ./data/rt/rt-data-2:/s      
      - ./data/rt/rt-data-session:/rt-session
      - ${local_dir}:${mnt_dir}
      - ./data/rt/node2:/tmp/node

  dap:
    image: ${kxi_da_single}
    ports: 
      - 5080:5080
      - 5081:5081
      - 5082:5082
      - 5083:5083 
    command: -p 5080
    environment:
      - TZ=Asia/Hong_Kong
      - KXI_NAME=dap
      - KXI_SC=dap
      - KXI_PORT=5080
      - KXI_ASSEMBLY_FILE=/mnt/cfg/assembly-rt.yaml
      - KXI_CUSTOM_FILE=/mnt/src/da/custom.q # Optional for custom APIs
      - KDB_LICENSE_B64
      - KXI_SECURE_ENABLED=false
      - KXI_LOG_CONFIG=/mnt/cfg/KXI_LOG_CONFIG/qlog.json
      - RT_LOG_PATH=/mnt/data/da/rt_log_path
      - RT_TOPIC_PREFIX
      - RT_EXTERN_PREFIX      
    volumes:
      - ${local_dir}:/mnt
    networks: [kx]
  sm:
    image: ${kxi_sm_single}
    ports: [20001:20001]
    command: -p 20001
    environment:
      - TZ=Asia/Hong_Kong      
      - KXI_NAME=sm
      - KXI_SC=SM
      - KXI_ASSEMBLY_FILE=/mnt/cfg/assembly-rt.yaml
      - KDB_LICENSE_B64
      - KXI_LOG_CONFIG=/mnt/cfg/KXI_LOG_CONFIG/qlog.json      
      - KXI_SECURE_ENABLED=false
      - RT_TOPIC_PREFIX
      - RT_EXTERN_PREFIX      
    volumes:
      - ${local_dir}:/mnt
      - ./data/sm/sm:/logs/rt/sm
      - ./data/sm/eoi:/logs/rt/eoi
    networks: [kx]
  sgrc:
    image: ${kxi_sg_rc}
    ports: [5050:5050]
    environment:
      - TZ=Asia/Hong_Kong      
      - KXI_NAME=sg_rc
      - KXI_PORT=5050
      - KDB_LICENSE_B64
      - KXI_LOG_CONFIG=/mnt/cfg/KXI_LOG_CONFIG/qlog.json 
      - KXI_ALLOWED_SBX_APIS=.kxi.sql,.kxi.qsql
      - KXI_SECURE_ENABLED=false
    networks: [kx]
    volumes:
      - ${local_dir}:/mnt
  sgagg:
    image: ${kxi_sg_agg}
    ports: [5060:5060]
    environment:
      - TZ=Asia/Hong_Kong      
      - KXI_NAME=sg_agg
      - KXI_PORT=5060
      - KXI_ASSEMBLY_FILE1=/mnt/cfg/assembly-rt.yaml      
      - KXI_SG_RC_ADDR=sgrc:5050
      - KXI_CUSTOM_FILE=/mnt/src/agg/custom.q # Optional for custom APIs
      - KDB_LICENSE_B64
      - KXI_LOG_CONFIG=/mnt/cfg/KXI_LOG_CONFIG/qlog.json 
      - KXI_SECURE_ENABLED=false
    deploy: # Optional: deploy multiple replicas.
      mode: replicated
      replicas: 1
    networks: [kx]
    volumes:
      - ${local_dir}:/mnt
  sggw:
    image: ${kxi_sg_gw}
    ports:
      - 5040:5040
      - 8080:8080
    environment:
      - TZ=Asia/Hong_Kong      
      - GATEWAY_QIPC_PORT=5040
      - GATEWAY_HTTP_PORT=8080
      - KXI_SG_RC_ADDR=sgrc:5050
      - KXI_LOG_CONFIG=/mnt/cfg/KXI_LOG_CONFIG/qlog.json 
      - KXI_SECURE_ENABLED=false
    deploy: # Optional: deploy multiple replicas.
      mode: replicated
      replicas: 1
    networks: [kx]
    volumes:
      - ${local_dir}:/mnt    

  sp0-controller:
    image: ${kxi_sp_controller}
    ports:
      - 6100:6100
    environment:
      - TZ=Asia/Hong_Kong      
      - KDB_LICENSE_B64
      - KXI_LOG_CONFIG=/mnt/cfg/KXI_LOG_CONFIG/qlog.json
      - RT_TOPIC_PREFIX
      - RT_EXTERN_PREFIX
    command: ["-p", "6100"]
    networks: [kx]
    volumes:
      - ${local_dir}:/mnt
      - ./data/sp0:/sp # fixed path for 

  sp0-worker:
    image: ${kxi_sp_worker}    
    ports:
      - 5020:5020
    environment:
      - TZ=Asia/Hong_Kong      
      - KXI_SP_SPEC=/mnt/src/sp0/sp.worker.rt.q                # Point to the bound spec.q file
      - KXI_SP_PARENT_HOST=sp0-controller:6100     # Point to the parent Controller
      - KXI_LOG_CONFIG=/mnt/cfg/KXI_LOG_CONFIG/qlog.json
      - KDB_LICENSE_B64
      - RT_TOPIC_PREFIX
      - RT_EXTERN_PREFIX
    command: ["-p", "5020"]
    networks: [kx]
    volumes:
      - ${local_dir}:/mnt
      - ./data/sp0:/sp      

  insideq:
    image: registry.dl.kx.com/qce:4.0.3
    ports: [8888:8888]    
    command: insideq.q -p 8888
    working_dir: /mnt
    environment:
      - TZ=Asia/Hong_Kong      
      - KDB_LICENSE_B64
      - BTSRC=/mnt/btick
      - KXI_LOG_CONFIG=/mnt/cfg/KXI_LOG_CONFIG/qlog.json      
    volumes:
      - ${local_dir}:/mnt
      - ${btick_dir}:/mnt/btick
    networks: [kx]      