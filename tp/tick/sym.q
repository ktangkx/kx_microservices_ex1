// internal tables
// with `time` and `sym` columns added by RT client for compatibility
(`$"_prtnEnd")set ([] time:"n"$(); sym:`$(); signal:`$(); endTS:"p"$(); opts:());
(`$"_reload")set ([] time:"n"$(); sym:`$(); mount:`$(); params:(); asm:`$())
(`$"_heartbeats")set ([] time:"n"$(); sym:`$(); foo:"j"$())


trade:flip`time`sym`realTime`price`size!`timespan`symbol`timestamp`float`long$\:()
quote:flip`time`sym`realTime`bid`ask`bidSize`askSize!`timespan`symbol`timestamp`float`float`long`long$\:()