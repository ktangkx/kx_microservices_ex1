cd C:\edev\work\kx_microservices\src
rm -rf data/logs/* data/da/* data/db/* data/sm/sm/* data/sm/eoi/* data/sp0/*
ls data/sp0
cd data/sp0
mkdir checkpoints
cd ../..

docker-compose -p ms-tp -f docker-compose-tp.yaml up --force-recreate -d
rem docker-compose -p ms-tp -f docker-compose-tp.yaml logs
rem docker-compose -p ms-tp -f docker-compose-tp.yaml top 
docker-compose -p ms-tp -f docker-compose-tp.yaml down


wscat -c ws://sggw:8080/ws/v1/subscribe/sp-spwork-x.firstderivatives.com
wscat -c ws://sggw:8080/ws/v1/subscribe/spwork-x.firstderivatives.com
wscat -c ws://localhost:5040/ws/v1/subscribe/sp0-worker
docker run -ti --rm mattdavis90/wscat -c ws://echo.websocket.org

curl sp0-controller:6100/pipelines
curl localhost:6100/pipelines
curl localhost:5020/pipelines