args:.Q.def[`name`port!("main-rt.q";9003);].Q.opt .z.x

/ remove this line when using in production
/ main-rt.q:localhost:9003::
{ if[not x=0; @[x;"\\\\";()]]; value"\\p 9003"; } @[hopen;`:localhost:9003;0];

\l qlib.q

.import.module`remote

.remote.add `uid`host`port`user`passwd!(`ms.rt.rt.data.0.admin;`localhost;6000;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.dap.super;`localhost;5080;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.dap.rdb;`localhost;5081;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.dap.idb;`localhost;5082;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.dap.hdb;`localhost;5083;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.sm.main;`localhost;20001;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.sgrc.main;`localhost;5050;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.sgagg.main;`localhost;5060;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.sggw.main;`localhost;5040;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.sggw.http;`localhost;8080;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.sp0.con;`localhost;6100;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.sp0.wor;`localhost;5020;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.insideq.main;`localhost;8888;`;enlist"")


/ .remote.sbl[]

b) rm -rf data/rt/rt-data-session/* data/rt/rt-data-0/* data/rt/node0/* data/rt/rt-data-1/* data/rt/node1/* data/rt/rt-data-2/* data/rt/node2/* data/da/* data/db/* data/sm/sm/* data/sm/eoi/* data/sp0/*

b) docker-compose -p ms -f docker-compose-rt.yaml up --force-recreate -d
b) docker-compose -p ms -f docker-compose-rt.yaml logs
b) docker-compose -p ms -f docker-compose-rt.yaml top 
b) docker-compose -p ms -f docker-compose-rt.yaml down


(::).f.proc:`ms.dap.rdb
(::)quote:"f" "quote"
(::)trade:"f" "trade"


.remote.fduplicate[`ms.sp0.wor] `.u.upd
(::).f.proc:`ms.sp0.wor
f) .qsp.teardown[]
f) .qsp.run (quStream;trStream)

.u.upd[`trade] `time`sym`realTime`price`size!(.z.N;`aaa;.z.P;100f;10j)
 
(::).f.proc:`ms.dap.rdb`ms.dap.idb
f)count select from trade