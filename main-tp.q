args:.Q.def[`name`port!("main-tp.q";9013);].Q.opt .z.x

/ remove this line when using in production
/ main-tp.q:localhost:9013::
{ if[not x=0; @[x;"\\\\";()]]; value"\\p 9013"; } @[hopen;`:localhost:9013;0];

/ 
 This process is not needed but might help you 
 To use this process you need to git clone the btick library : https://github.com/kimtang/btick
 $ cd C:\edev\private\btick
 $ git clone https://github.com/kimtang/btick src
 $ set btsrc=C:\edev\private\btick\src
 Then you need to modify the .env file to point btick_dir variable to the btick library.
\ 



\l qlib.q

.import.module`remote

.remote.add `uid`host`port`user`passwd!(`ms.tp.tp;`localhost;5010;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.tp.rdb;`localhost;5011;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.dap.super;`localhost;5080;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.dap.rdb;`localhost;5081;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.dap.idb;`localhost;5082;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.dap.hdb;`localhost;5083;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.sm.main;`localhost;20001;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.sgrc.main;`localhost;5050;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.sgagg.main;`localhost;5060;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.sggw.main;`localhost;5040;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.sggw.http;`localhost;8080;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.sp0.con;`localhost;6100;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.sp0.wor;`localhost;5020;`;enlist"")
.remote.add `uid`host`port`user`passwd!(`ms.insideq.main;`localhost;8888;`;enlist"")


/ .remote.sbl[]

b) rm -rf data/logs/* data/da/* data/db/* data/sm/sm/* data/sm/eoi/* data/sp0/*

b) docker-compose -p ms-tp -f docker-compose-tp.yaml up --force-recreate -d
b) docker-compose -p ms-tp -f docker-compose-tp.yaml logs
b) docker-compose -p ms-tp -f docker-compose-tp.yaml top 
b) docker-compose -p ms-tp -f docker-compose-tp.yaml down

(::).f.proc:`ms.tp.tp
f).poc.con
f).poc.pgs

(::).f.proc:`ms.tp.rdb
(::)quote:"f" "quote"
(::)trade:"f" "trade"


.remote.fduplicate[`ms.sp0.wor] `.u.upd
(::).f.proc:`ms.sp0.wor

.u.upd[`trade] (`aaa;.z.P;100f;10j)
.u.upd[`quote] (`bbb;.z.P;100f;200f;1000j;2000j)
 

(::).f.proc:`ms.dap.rdb`ms.dap.idb`ms.tp.rdb
f)count select from trade
f)count select from quote

.remote.fduplicate[`ms.insideq.main] @'`.kxi.getMeta`.kxi.getData
(::)get last `meta0`data0 set' .kxi.getMeta[;`;(0#`)!()] `region`startTS`endTS!(`HKG;-0Wp;0Wp)
(::)get last `rmeta`rdata set' result:.kxi.getData[;`;(1#`timeout)!(1#10000)] args:`table`startTS`endTS!(`trade;"p"$.z.D;"p"$.z.D+1)
(::)get last `rmeta`rdata set' result:.kxi.getData[;`;(1#`timeout)!(1#10000)] args:`table`startTS`endTS!(`quote;"p"$.z.D;"p"$.z.D+1)